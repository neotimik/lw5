package com.human.lw5;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by human on 04.04.18.
 */

public class SQLHelper extends SQLiteOpenHelper {

    public SQLHelper(Context context) {
        super(context, "TrainBase", null, 3);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + "people (id integer primary key autoincrement, name text not null, age integer not null, receiptTime text);");
        db.execSQL("create table " + "doctors (id integer primary key autoincrement, name text not null," +
                " specialization text not null, workDayStart text not null, workDayEnd text not null );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists people");
        db.execSQL("drop table if exists doctors");
        onCreate(db);
    }

    public Cursor getFullPeopleTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.query("people", new String[]{"id", "name", "age", "receiptTime"}, null, null, null, null, null);
    }
    public Cursor getFullDoctorsTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.query("doctors", new String[]{"id", "name", "specialization", "workDayStart", "workDayEnd"}, null, null, null, null, null);
    }

    public void removePeople() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("people",  null, null);
    }


}
