package com.human.lw5;

/**
 * Created by human on 04.04.18.
 */

public class Person {
    private int _id;
    private String name;
    private int age;
    private String receiptTime;

    public Person(int _id, String name, int age, String receiptTime) {
        this._id = _id;
        this.name = name;
        this.age = age;
        this.receiptTime = receiptTime;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getReceiptTime() {
        return receiptTime;
    }

    public void setReceiptTime(String receiptTime) {
        this.receiptTime = receiptTime;
    }
}
