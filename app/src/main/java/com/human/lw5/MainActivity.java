package com.human.lw5;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SQLHelper db;
    Button savePersonBtn;
    Button removePeople;
    TimePicker timePicker;
    LinearLayout row;
    LinearLayout peopleContainer;
    LinearLayout doctorsContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new SQLHelper(this);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        savePersonBtn = (Button) findViewById(R.id.savePersonButton);
//        db = fillPeopleTable(db);
//        db = fillDoctorsTable(db);
        peopleContainer = (LinearLayout) findViewById(R.id.peopleContainer);
        doctorsContainer = (LinearLayout) findViewById(R.id.doctorsContainer);
        removePeople = (Button) findViewById(R.id.removePeopleButton);
        displayPeople();
        displayDoctors();

    }

    public ArrayList<Person> getPeopleList() {
        ArrayList<Person> people = new ArrayList<>();
        Cursor cursor = db.getFullPeopleTable();
        int i = 0;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                people.add(new Person(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3)));
                i++;
            }
        }
        return people;
    }
    public ArrayList<Doctor> getDoctorsList() {
        ArrayList<Doctor> doctors = new ArrayList<>();
        Cursor cursor = db.getFullDoctorsTable();
        int i = 0;
        if (cursor != null) {
            while (cursor.moveToNext() ) {
                doctors.add(new Doctor(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
                i++;
            }
        }
        return doctors;
    }


    public SQLHelper fillPeopleTable(SQLHelper db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "Тим");
        contentValues.put("age", "22");
        db.getWritableDatabase().insert("people", null, contentValues);
        return db;
    }

    public void addToPeopleTable(SQLHelper db, String name ,String age, String time) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("age", age);
        contentValues.put("receiptTime", time);
        db.getWritableDatabase().insert("people", null, contentValues);
//        return db;
    }


    public SQLHelper fillDoctorsTable(SQLHelper db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", "Иван");
        contentValues.put("specialization", "Терапевт");
        contentValues.put("workDayStart", "9:00");
        contentValues.put("workDayEnd", "18:00");
        db.getWritableDatabase().insert("doctors", null, contentValues);
        return db;
    }


    public void savePerson(View view) {
        EditText nameField = (EditText) findViewById(R.id.nameEditText);
        EditText ageField = (EditText) findViewById(R.id.ageEditText);
        if (String.valueOf(nameField.getText()).equals("") || String.valueOf(ageField.getText()).equals("") ) {
            Toast.makeText(this, "Необходимо заполнить все поля", Toast.LENGTH_SHORT).show();
            return;
        }
        String receiptTime = String.valueOf(timePicker.getHour()) + ":" + String.valueOf(timePicker.getMinute());
        addToPeopleTable(db, nameField.getText().toString(), ageField.getText().toString(), receiptTime);
        Toast.makeText(this, "Добавлено", Toast.LENGTH_SHORT).show();

        displayPeople();


    }

    public void removePeople(View view) {
        db.removePeople();
        displayPeople();
    }

    public void displayPeople() {
        peopleContainer.removeAllViews();
        row = new LinearLayout(this);
        row.setOrientation(LinearLayout.HORIZONTAL);


        TextView idTr = new TextView(this);
        idTr.setText("ID");
        idTr.setWidth(60);

        TextView nameTr = new TextView(this);
        nameTr.setText("Name");
        nameTr.setWidth(220);

        TextView ageTr = new TextView(this);
        ageTr.setText(String.valueOf("Age"));
        ageTr.setWidth(220);

        TextView receiptTimeTr = new TextView(this);
        receiptTimeTr.setText(String.valueOf("time"));
        receiptTimeTr.setWidth(220);

        row.addView(idTr);
        row.addView(nameTr);
        row.addView(ageTr);
        row.addView(receiptTimeTr);
        peopleContainer.addView(row);



        ArrayList<Person> people = getPeopleList();
        for (Person person: people) {
            row = new LinearLayout(this);
            row.setOrientation(LinearLayout.HORIZONTAL);


            TextView id = new TextView(this);
            id.setText(String.valueOf(person.get_id()));
            id.setWidth(60);

            TextView name = new TextView(this);
            name.setText(person.getName());
            name.setWidth(220);

            TextView age = new TextView(this);
            age.setText(String.valueOf(person.getAge()));
            age.setWidth(220);

            TextView receiptTime = new TextView(this);
            receiptTime.setText(String.valueOf(person.getReceiptTime()));
            receiptTime.setWidth(220);

            row.addView(id);
            row.addView(name);
            row.addView(age);
            row.addView(receiptTime);
            peopleContainer.addView(row);
        }
    }

    public void displayDoctors() {

        doctorsContainer.removeAllViews();
        row = new LinearLayout(this);
        row.setOrientation(LinearLayout.HORIZONTAL);


        TextView idTr = new TextView(this);
        idTr.setText(String.valueOf("ID"));
        idTr.setWidth(60);

        TextView nameTr = new TextView(this);
        nameTr.setText("Name");
        nameTr.setWidth(220);

        TextView specializationTr = new TextView(this);
        specializationTr.setText("Spec");
        specializationTr.setWidth(220);

        TextView workDayStartTr = new TextView(this);
        workDayStartTr.setText("start");
        workDayStartTr.setWidth(220);

        TextView workDayEndTr = new TextView(this);
        workDayEndTr.setText("end");
        workDayEndTr.setWidth(220);


        row.addView(idTr);
        row.addView(nameTr);
        row.addView(specializationTr);
        row.addView(workDayStartTr);
        row.addView(workDayEndTr);
        doctorsContainer.addView(row);

        ArrayList<Doctor> doctors = getDoctorsList();

        for (Doctor doctor: doctors) {
            row = new LinearLayout(this);
            row.setOrientation(LinearLayout.HORIZONTAL);


            TextView id = new TextView(this);
            id.setText(String.valueOf(doctor.getId()));
            id.setWidth(60);

            TextView name = new TextView(this);
            name.setText(doctor.getName());
            name.setWidth(220);

            TextView specialization = new TextView(this);
            specialization.setText(doctor.getSpecialization());
            specialization.setWidth(220);

            TextView workDayStart = new TextView(this);
            workDayStart.setText(doctor.getWorkDayStart());
            workDayStart.setWidth(220);

            TextView workDayEnd = new TextView(this);
            workDayEnd.setText(doctor.getWorkDayEnd());
            workDayEnd.setWidth(220);


            row.addView(id);
            row.addView(name);
            row.addView(specialization);
            row.addView(workDayStart);
            row.addView(workDayEnd);
            doctorsContainer.addView(row);
        }
    }

}
