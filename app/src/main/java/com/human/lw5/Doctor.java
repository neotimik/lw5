package com.human.lw5;

/**
 * Created by human on 05.04.18.
 */

public class Doctor {

    private int id;
    private String name;
    private String specialization;
    private String workDayStart;
    private String workDayEnd;

    public Doctor(int id, String name, String specialization, String workDayStart, String workDayEnd) {
        this.id = id;
        this.name = name;
        this.specialization = specialization;
        this.workDayStart = workDayStart;
        this.workDayEnd = workDayEnd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getWorkDayStart() {
        return workDayStart;
    }

    public void setWorkDayStart(String workDayStart) {
        this.workDayStart = workDayStart;
    }

    public String getWorkDayEnd() {
        return workDayEnd;
    }

    public void setWorkDayEnd(String workDayEnd) {
        this.workDayEnd = workDayEnd;
    }
}
